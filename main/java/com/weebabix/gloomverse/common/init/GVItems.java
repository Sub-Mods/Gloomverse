package com.weebabix.gloomverse.common.init;

import java.util.ArrayList;
import java.util.List;

import com.weebabix.gloomverse.client.creativetabs.GVCreativeTabs;
import com.weebabix.gloomverse.common.items.ItemAssistantHat;
import com.weebabix.gloomverse.common.items.ItemHandWeapon;
import com.weebabix.gloomverse.common.items.ItemHaroldHat;
import com.weebabix.gloomverse.common.items.ItemMagicBook;
import com.weebabix.gloomverse.common.items.ItemWallisHat;
import com.weebabix.gloomverse.common.misc.GVMaterials;
import com.weebabix.gloomverse.main.Gloomverse;
import com.weebabix.gloomverse.main.proxy.CommonProxy;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.registry.GameRegistry;


public class GVItems {
	public static List<Item> Items = new ArrayList();
	
	public static Item CHaroldHat;
	public static Item iMagicBook;	
	public static Item WLollipop;
	public static Item CWallisHat;
	private static Item CassistHat;
	
	
	public static void init() 
	{
		GVItems.AddItems();
	}
		
	public static void AddItems() 
	{
		CHaroldHat = Add(new ItemHaroldHat(ArmorMaterial.LEATHER, 0, EntityEquipmentSlot.HEAD).setCreativeTab(GVCreativeTabs.GloomverseClothing), "charoldhat");		
		iMagicBook = Add(new ItemMagicBook().setCreativeTab(GVCreativeTabs.GloomverseItems), "imagicbook");
		WLollipop = Add(new ItemHandWeapon(GVMaterials.MAT_LOLLIPOP).setCreativeTab(GVCreativeTabs.GloomverseItems), "wlollipop");		
		CWallisHat = Add(new ItemWallisHat(ArmorMaterial.LEATHER, 0, EntityEquipmentSlot.HEAD).setCreativeTab(GVCreativeTabs.GloomverseClothing), "cwallishat");
		CassistHat = Add(new ItemAssistantHat(ArmorMaterial.LEATHER, 0, EntityEquipmentSlot.HEAD).setCreativeTab(GVCreativeTabs.GloomverseClothing), "cassisthat");
	}
	
	
	private static Item Add(Item RegItem, String name) {
		Item item = RegItem;
		RegItem.setUnlocalizedName(name);
		RegItem.setRegistryName(name);
		GameRegistry.register(item);
		Items.add(item);
		return item;
	}
	

}
