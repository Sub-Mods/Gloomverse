package com.weebabix.gloomverse.common.misc;

import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;

public class GVMaterials {

	//Created this class so the items class doesn't get so messy...
	//Kinda makes it easier for me to explain to you though!
	//Basically: ("LOLLIPOP", 2, 600, 2.0f, 5, 120);
	//Means (Material Name, Haverst Level, Max uses, efficiency, Damage to enemy, Enchantibility)
	
	public static ToolMaterial MAT_LOLLIPOP = EnumHelper.addToolMaterial("LOLLIPOP", 2, 600, 2.0f, 5, 120);
	
}
