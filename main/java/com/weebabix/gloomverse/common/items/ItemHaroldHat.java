package com.weebabix.gloomverse.common.items;

import java.util.List;

import com.weebabix.gloomverse.client.models.clothing.ModelHaroldHat;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemHaroldHat extends ItemArmor {
	
	public ItemHaroldHat(ArmorMaterial materialIn, int renderIndexIn, EntityEquipmentSlot equipmentSlotIn) {
		super(materialIn, renderIndexIn, equipmentSlotIn);
	
	}

	@Override 	
	@SideOnly(Side.CLIENT)
		public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack stack, EntityEquipmentSlot armorSlot,
				ModelBiped defaultModel) {
		
		        ModelBase modelHat = new ModelHaroldHat();

			if (stack != null) {
				if (stack.getItem() instanceof ItemArmor) {

					EntityEquipmentSlot type = ((ItemArmor) stack.getItem()).armorType;
					ModelBiped armorModel = (ModelBiped) modelHat;

					armorModel.bipedHead.showModel = armorSlot == EntityEquipmentSlot.HEAD;
					armorModel.bipedHeadwear.showModel = armorSlot == EntityEquipmentSlot.HEAD;
					armorModel.bipedBody.showModel = (armorSlot == EntityEquipmentSlot.CHEST)
							|| (armorSlot == EntityEquipmentSlot.CHEST);
					armorModel.bipedRightArm.showModel = armorSlot == EntityEquipmentSlot.CHEST;
					armorModel.bipedLeftArm.showModel = armorSlot == EntityEquipmentSlot.CHEST;
					armorModel.bipedRightLeg.showModel = (armorSlot == EntityEquipmentSlot.LEGS)
							|| (armorSlot == EntityEquipmentSlot.FEET);
					armorModel.bipedLeftLeg.showModel = (armorSlot == EntityEquipmentSlot.LEGS)
							|| (armorSlot == EntityEquipmentSlot.FEET);

					armorModel.isSneak = defaultModel.isSneak;
					armorModel.isRiding = defaultModel.isRiding;
					armorModel.isChild = defaultModel.isChild;
					armorModel.rightArmPose = defaultModel.rightArmPose;
					armorModel.leftArmPose = defaultModel.leftArmPose;

					return armorModel;
				}
			}
			return null;
	}
	
	
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List tooltip, boolean advanced) {
		super.addInformation(stack, playerIn, tooltip, advanced);
	}
	
	@Override
	public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) {
		super.onCreated(stack, worldIn, playerIn);
	}

}