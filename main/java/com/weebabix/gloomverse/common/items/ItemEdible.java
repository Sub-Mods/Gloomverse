package com.weebabix.gloomverse.common.items;

import net.minecraft.item.ItemFood;

public class ItemEdible extends ItemFood {	

	public ItemEdible(int amount, float saturation, boolean isWolfFood) {
		super(amount, saturation, isWolfFood);
	}
}


