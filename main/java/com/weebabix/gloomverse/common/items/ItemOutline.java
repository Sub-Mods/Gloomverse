package com.weebabix.gloomverse.common.items;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemOutline extends Item {
	String[] description;

	public ItemOutline(String... desc) {
		this.description = desc;
	}

	public ItemOutline(String desc) {
		String[] str = { desc };
		this.description = str;
	}

	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List tooltip, boolean advanced) {
		for (String ItemDescription : description) {
			tooltip.add(ItemDescription);
		}
		super.addInformation(stack, playerIn, tooltip, advanced);
	}
}
