package com.weebabix.gloomverse.common.items;

import java.awt.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemHandWeapon extends ItemSword {
	
	
	public ItemHandWeapon(ToolMaterial material) {
		super(material);
	}
	
}