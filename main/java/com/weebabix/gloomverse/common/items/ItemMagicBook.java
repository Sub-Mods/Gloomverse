package com.weebabix.gloomverse.common.items;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemMagicBook extends Item {
	
	@Override
	   public Item setMaxStackSize(int maxStackSize)
    {
        this.maxStackSize = 0;
        return this;
    }
	
	
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List tooltip, boolean advanced) {
			tooltip.add("Book of magic!");
		super.addInformation(stack, playerIn, tooltip, advanced);
	}
}
