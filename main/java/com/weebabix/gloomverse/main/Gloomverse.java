package com.weebabix.gloomverse.main;

 
import com.weebabix.gloomverse.client.creativetabs.GVCreativeTabs;
import com.weebabix.gloomverse.common.init.GVItems;
import com.weebabix.gloomverse.main.proxy.CommonProxy;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = "gloomversemagic", name = "Weebabix's Gloomverse Mod", version = "1.0", acceptedMinecraftVersions = "[1.10.2]")

public class Gloomverse {
	
	public static final String MODID = "gloomversemagic";
	
	@Instance
	public static Gloomverse instance;
	
	@SidedProxy(clientSide = "com.weebabix.gloomverse.main.proxy.ClientProxy", serverSide = "com.weebabix.gloomverse.main.proxy.CommonProxy")
    
	public static CommonProxy TheProxy;
	
	@EventHandler
	public void PreInit(FMLPreInitializationEvent event)
	{
		GVItems.init();
	}
	
	@EventHandler
	public void Init(FMLInitializationEvent event)
	{
		TheProxy.Init();
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{

	}
	
	


}
