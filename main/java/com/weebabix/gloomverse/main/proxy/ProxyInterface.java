package com.weebabix.gloomverse.main.proxy;

public interface ProxyInterface
{
	public void Init();
	
	public void preInit();
	
	public boolean isSinglePlayer();

	public boolean isDedicatedServer();

}