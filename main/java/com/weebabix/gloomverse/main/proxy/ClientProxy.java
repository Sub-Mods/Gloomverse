package com.weebabix.gloomverse.main.proxy;

import com.weebabix.gloomverse.common.init.GVItems;
import com.weebabix.gloomverse.main.Gloomverse;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;

public class ClientProxy extends CommonProxy
{
	@Override
	public void Init(){	
		RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
		ItemRendering(renderItem);	
	}
	
	private void ItemRendering(RenderItem render) {
		for (int i = 0; i < GVItems.Items.size(); i++) {
			Item item = GVItems.Items.get(i);
				render.getItemModelMesher().register(item, 0, new ModelResourceLocation(Gloomverse.MODID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
			}
		}
	
}