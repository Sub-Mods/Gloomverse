package com.weebabix.gloomverse.client.creativetabs;

import com.weebabix.gloomverse.common.init.GVItems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class GVCreativeTabs {
		
	
	//Tab Start.
	public static CreativeTabs GloomverseClothing = new CreativeTabs("GloomverseClothing") {
		@Override
		public Item getTabIconItem() {
			return(GVItems.CHaroldHat);
		}	
	};
	//Tab End - Simples!
	
	
	public static CreativeTabs GloomverseItems = new CreativeTabs("GloomverseItems") {
		@Override
		public Item getTabIconItem() {
			return(GVItems.iMagicBook);
		}	
	};	
	
}

