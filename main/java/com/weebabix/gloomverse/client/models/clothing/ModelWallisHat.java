package com.weebabix.gloomverse.client.models.clothing;

import org.lwjgl.opengl.GL11;

import com.weebabix.gloomverse.main.Gloomverse;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelWallisHat extends ModelBiped {
	public static final ResourceLocation Hat = new ResourceLocation(
			Gloomverse.MODID + ":textures/models/armor/wallishat.png");

    ModelRenderer Shape7;
    ModelRenderer Shape8;
    ModelRenderer Shape9;
    ModelRenderer Shape10;
    ModelRenderer Shape11;
    ModelRenderer Shape12;
    ModelRenderer Shape13;
    ModelRenderer Shape14;
    ModelRenderer Shape15;
    ModelRenderer Shape16;
    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape5;
    ModelRenderer Shape4;
    ModelRenderer Shape6;

	public ModelWallisHat() {
		super();
		textureWidth = 128;
		textureHeight = 128;

		  Shape7 = new ModelRenderer(this, 0, 30);
	      Shape7.addBox(0F, 0F, 0F, 1, 1, 10);
	      Shape7.setRotationPoint(-7F, -6F, -5F);
	      Shape7.setTextureSize(64, 32);
	      Shape7.mirror = true;
	      setRotation(Shape7, 0F, 0F, 0F);
	      Shape8 = new ModelRenderer(this, 22, 30);
	      Shape8.addBox(0F, 0F, 0F, 1, 1, 10);
	      Shape8.setRotationPoint(6F, -6F, -5F);
	      Shape8.setTextureSize(64, 32);
	      Shape8.mirror = true;
	      setRotation(Shape8, 0F, 0F, 0F);
	      Shape9 = new ModelRenderer(this, 48, 16);
	      Shape9.addBox(0F, 0F, 0F, 10, 1, 1);
	      Shape9.setRotationPoint(-5F, -6F, 6F);
	      Shape9.setTextureSize(64, 32);
	      Shape9.mirror = true;
	      setRotation(Shape9, 0F, 0F, 0F);
	      Shape10 = new ModelRenderer(this, 70, 16);
	      Shape10.addBox(0F, 0F, 0F, 10, 1, 1);
	      Shape10.setRotationPoint(-5F, -6F, -7F);
	      Shape10.setTextureSize(64, 32);
	      Shape10.mirror = true;
	      setRotation(Shape10, 0F, 0F, 0F);
	      Shape11 = new ModelRenderer(this, 1, 41);
	      Shape11.addBox(-0.5F, 0F, -0.5F, 1, 1, 4);
	      Shape11.setRotationPoint(5F, -9F, -4F);
	      Shape11.setTextureSize(64, 32);
	      Shape11.mirror = true;
	      setRotation(Shape11, 0F, 0F, 0F);
	      Shape12 = new ModelRenderer(this, 11, 41);
	      Shape12.addBox(-0.5F, 0F, 0F, 1, 4, 1);
	      Shape12.setRotationPoint(5F, -10F, -3F);
	      Shape12.setTextureSize(64, 32);
	      Shape12.mirror = true;
	      setRotation(Shape12, 0.2974289F, 0F, 0F);
	      Shape13 = new ModelRenderer(this, 15, 41);
	      Shape13.addBox(-0.5F, -0.3F, 0F, 1, 4, 1);
	      Shape13.setRotationPoint(5F, -10F, -3F);
	      Shape13.setTextureSize(64, 32);
	      Shape13.mirror = true;
	      setRotation(Shape13, -0.2974216F, 0F, 0F);
	      Shape14 = new ModelRenderer(this, 20, 41);
	      Shape14.addBox(-0.5F, 0F, 0F, 1, 1, 3);
	      Shape14.setRotationPoint(5F, -8F, 0F);
	      Shape14.setTextureSize(64, 32);
	      Shape14.mirror = true;
	      setRotation(Shape14, 0F, 0F, 0F);
	      Shape15 = new ModelRenderer(this, 28, 41);
	      Shape15.addBox(-0.5F, 0F, 0F, 1, 3, 1);
	      Shape15.setRotationPoint(5F, -9F, 1F);
	      Shape15.setTextureSize(64, 32);
	      Shape15.mirror = true;
	      setRotation(Shape15, 0.2974216F, 0F, 0F);
	      Shape16 = new ModelRenderer(this, 32, 41);
	      Shape16.addBox(-0.5F, -0.3F, 0F, 1, 3, 1);
	      Shape16.setRotationPoint(5F, -9F, 1F);
	      Shape16.setTextureSize(64, 32);
	      Shape16.mirror = true;
	      setRotation(Shape16, -0.2974216F, 0F, 0F);
	      Shape1 = new ModelRenderer(this, 32, 0);
	      Shape1.addBox(0F, 0F, 0F, 10, 7, 1);
	      Shape1.setRotationPoint(-5F, -13F, -5F);
	      Shape1.setTextureSize(64, 32);
	      Shape1.mirror = true;
	      setRotation(Shape1, 0F, 0F, 0F);
	      Shape2 = new ModelRenderer(this, 54, 0);
	      Shape2.addBox(0F, 0F, 0F, 10, 7, 1);
	      Shape2.setRotationPoint(-5F, -13F, 4F);
	      Shape2.setTextureSize(64, 32);
	      Shape2.mirror = true;
	      setRotation(Shape2, 0F, 0F, 0F);
	      Shape3 = new ModelRenderer(this, 76, 0);
	      Shape3.addBox(0F, 0F, 0F, 1, 7, 8);
	      Shape3.setRotationPoint(4F, -13F, -4F);
	      Shape3.setTextureSize(64, 32);
	      Shape3.mirror = true;
	      setRotation(Shape3, 0F, 0F, 0F);
	      Shape5 = new ModelRenderer(this, 32, 8);
	      Shape5.addBox(0F, 0F, 0F, 8, 1, 8);
	      Shape5.setRotationPoint(-4F, -13F, -4F);
	      Shape5.setTextureSize(64, 32);
	      Shape5.mirror = true;
	      setRotation(Shape5, 0F, 0F, 0F);
	      Shape4 = new ModelRenderer(this, 94, 0);
	      Shape4.addBox(0F, 0F, 0F, 1, 7, 8);
	      Shape4.setRotationPoint(-5F, -13F, -4F);
	      Shape4.setTextureSize(64, 32);
	      Shape4.mirror = true;
	      setRotation(Shape4, 0F, 0F, 0F);
	      Shape6 = new ModelRenderer(this, 0, 16);
	      Shape6.addBox(0F, 0F, 0F, 12, 1, 12);
	      Shape6.setRotationPoint(-6F, -6F, -6F);
	      Shape6.setTextureSize(64, 32);
	      Shape6.mirror = true;
	      setRotation(Shape6, 0F, 0F, 0F);

		this.bipedHeadwear.isHidden = true;

		this.bipedHead = new ModelRendererHook(this, 0, 0);
		this.bipedHead.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0);
		this.bipedHead.setRotationPoint(0.0F, 0.0F + 0, 0.0F);

		((ModelRendererHook) this.bipedHead).setRender(new IRender() {
			@Override
			public void render(float f5) {
				Minecraft.getMinecraft().renderEngine.bindTexture(Hat);
				Shape7.render(f5);
				Shape8.render(f5);
				Shape9.render(f5);
				Shape10.render(f5);
				Shape11.render(f5);
				Shape12.render(f5);
				Shape13.render(f5);
				Shape14.render(f5);
				Shape15.render(f5);
				Shape16.render(f5);
				Shape1.render(f5);
				Shape2.render(f5);
				Shape3.render(f5);
				Shape5.render(f5);
				Shape4.render(f5);
				Shape6.render(f5);
			}
		});
	}

	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		super.render(entity, f, f1, f2, f3, f4, f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public static interface IRender {
		public void render(float f);
	}

	public static class ModelRendererHook extends ModelRenderer {

		public IRender render;

		public ModelRendererHook(ModelBase p_i1172_1_, String p_i1172_2_) {
			super(p_i1172_1_, p_i1172_2_);
			// TODO Auto-generated constructor stub
		}

		public ModelRendererHook(ModelBase p_i46358_1_, int p_i46358_2_, int p_i46358_3_) {
			super(p_i46358_1_, p_i46358_2_, p_i46358_3_);
			// TODO Auto-generated constructor stub
		}

		public ModelRendererHook(ModelBase p_i1173_1_) {
			super(p_i1173_1_);
			// TODO Auto-generated constructor stub
		}

		public void setRender(IRender render) {
			this.render = render;
		}

		public IRender getRender() {
			return render;
		}

		@Override
		public void render(float p_78785_1_) {
			if (!this.isHidden) {
				if (this.showModel) {

					GlStateManager.translate(this.offsetX, this.offsetY, this.offsetZ);
					int i;

					if (this.rotateAngleX == 0.0F && this.rotateAngleY == 0.0F && this.rotateAngleZ == 0.0F) {
						if (this.rotationPointX == 0.0F && this.rotationPointY == 0.0F && this.rotationPointZ == 0.0F) {
							getRender().render(p_78785_1_);
						} else {
							GlStateManager.translate(this.rotationPointX * p_78785_1_, this.rotationPointY * p_78785_1_,
									this.rotationPointZ * p_78785_1_);

							getRender().render(p_78785_1_);

							GlStateManager.translate(-this.rotationPointX * p_78785_1_,
									-this.rotationPointY * p_78785_1_, -this.rotationPointZ * p_78785_1_);
						}
					} else {
						GlStateManager.pushMatrix();
						GlStateManager.translate(this.rotationPointX * p_78785_1_, this.rotationPointY * p_78785_1_,
								this.rotationPointZ * p_78785_1_);

						if (this.rotateAngleZ != 0.0F) {
							GlStateManager.rotate(this.rotateAngleZ * (180F / (float) Math.PI), 0.0F, 0.0F, 1.0F);
						}

						if (this.rotateAngleY != 0.0F) {
							GlStateManager.rotate(this.rotateAngleY * (180F / (float) Math.PI), 0.0F, 1.0F, 0.0F);
						}

						if (this.rotateAngleX != 0.0F) {
							GlStateManager.rotate(this.rotateAngleX * (180F / (float) Math.PI), 1.0F, 0.0F, 0.0F);
						}

						GL11.glScalef(1.025f, 1.025f, 1.025f);
						getRender().render(p_78785_1_);

						GlStateManager.popMatrix();
					}

					GlStateManager.translate(-this.offsetX, -this.offsetY, -this.offsetZ);
				}
			}
		}

	}

}
