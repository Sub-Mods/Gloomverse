package com.weebabix.gloomverse.client.models.clothing;

import org.lwjgl.opengl.GL11;

import com.weebabix.gloomverse.main.Gloomverse;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelAssistantHat extends ModelBiped
{
	public static final ResourceLocation Hat = new ResourceLocation(Gloomverse.MODID +":textures/models/armor/assishat.png");

	//fields
    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape4;
    ModelRenderer Shape5;
    ModelRenderer Shape6;
    ModelRenderer Shape7;
    ModelRenderer Shape8;
    ModelRenderer Shape9;
    ModelRenderer Shape10;

	public ModelAssistantHat()
	{
		super();
		 textureWidth = 128;
		    textureHeight = 128;
		    
		      Shape1 = new ModelRenderer(this, 24, 0);
		      Shape1.addBox(0F, 0F, 0F, 8, 1, 8);
		      Shape1.setRotationPoint(-4F, -9F, -4F);
		      Shape1.setTextureSize(64, 32);
		      Shape1.mirror = true;
		      setRotation(Shape1, 0F, 0F, 0F);
		      Shape2 = new ModelRenderer(this, 0, 0);
		      Shape2.addBox(0F, 0F, 0F, 6, 6, 6);
		      Shape2.setRotationPoint(-3F, -15F, -3F);
		      Shape2.setTextureSize(64, 32);
		      Shape2.mirror = true;
		      setRotation(Shape2, 0F, 0F, 0F);
		      Shape3 = new ModelRenderer(this, 0, 12);
		      Shape3.addBox(0F, 0F, 0F, 6, 1, 1);
		      Shape3.setRotationPoint(-3F, -9F, -5F);
		      Shape3.setTextureSize(64, 32);
		      Shape3.mirror = true;
		      setRotation(Shape3, 0F, 0F, 0F);
		      Shape4 = new ModelRenderer(this, 0, 14);
		      Shape4.addBox(0F, 0F, 0F, 6, 1, 1);
		      Shape4.setRotationPoint(-3F, -9F, 4F);
		      Shape4.setTextureSize(64, 32);
		      Shape4.mirror = true;
		      setRotation(Shape4, 0F, 0F, 0F);
		      Shape5 = new ModelRenderer(this, 56, 0);
		      Shape5.addBox(0F, 0F, 0F, 1, 1, 6);
		      Shape5.setRotationPoint(4F, -9F, -3F);
		      Shape5.setTextureSize(64, 32);
		      Shape5.mirror = true;
		      setRotation(Shape5, 0F, 0F, 0F);
		      Shape6 = new ModelRenderer(this, 70, 0);
		      Shape6.addBox(0F, 0F, 0F, 1, 1, 6);
		      Shape6.setRotationPoint(-5F, -9F, -3F);
		      Shape6.setTextureSize(64, 32);
		      Shape6.mirror = true;
		      setRotation(Shape6, 0F, 0F, 0F);
		      Shape7 = new ModelRenderer(this, 84, 0);
		      Shape7.addBox(-0.2F, 0F, 0F, 1, 1, 6);
		      Shape7.setRotationPoint(-3F, -10F, -3F);
		      Shape7.setTextureSize(64, 32);
		      Shape7.mirror = true;
		      setRotation(Shape7, 0F, 0F, 0F);
		      Shape8 = new ModelRenderer(this, 98, 0);
		      Shape8.addBox(0.2F, 0F, 0F, 1, 1, 6);
		      Shape8.setRotationPoint(2F, -10F, -3F);
		      Shape8.setTextureSize(64, 32);
		      Shape8.mirror = true;
		      setRotation(Shape8, 0F, 0F, 0F);
		      Shape9 = new ModelRenderer(this, 56, 7);
		      Shape9.addBox(0F, 0F, 0.2F, 6, 1, 1);
		      Shape9.setRotationPoint(-3F, -10F, 2F);
		      Shape9.setTextureSize(64, 32);
		      Shape9.mirror = true;
		      setRotation(Shape9, 0F, 0F, 0F);
		      Shape10 = new ModelRenderer(this, 70, 7);
		      Shape10.addBox(0F, 0F, -0.2F, 6, 1, 1);
		      Shape10.setRotationPoint(-3F, -10F, -3F);
		      Shape10.setTextureSize(64, 32);
		      Shape10.mirror = true;
		      setRotation(Shape10, 0F, 0F, 0F);
		      
		      
		this.bipedHeadwear.isHidden = true;

		this.bipedHead = new ModelRendererHookHarold(this, 0, 0);
		this.bipedHead.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0);
		this.bipedHead.setRotationPoint(0.0F, 0.0F + 0, 0.0F);

		((ModelRendererHookHarold)this.bipedHead).setRender(new IRender() {
			@Override
			public void render(float f5) {
				Minecraft.getMinecraft().renderEngine.bindTexture(Hat);
				Shape1.render(f5);
				Shape2.render(f5);
				Shape3.render(f5);
				Shape4.render(f5);
				Shape5.render(f5);
				Shape6.render(f5);
				Shape7.render(f5);
				Shape8.render(f5);
				Shape9.render(f5);
				Shape10.render(f5);

			}
		});
	}

	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public static interface IRender{
		public void render(float f);
	}

	public static class ModelRendererHookHarold extends ModelRenderer{

		public IRender render;

		public ModelRendererHookHarold(ModelBase p_i1172_1_, String p_i1172_2_) {
			super(p_i1172_1_, p_i1172_2_);
			// TODO Auto-generated constructor stub
		}

		public ModelRendererHookHarold(ModelBase p_i46358_1_, int p_i46358_2_, int p_i46358_3_) {
			super(p_i46358_1_, p_i46358_2_, p_i46358_3_);
			// TODO Auto-generated constructor stub
		}

		public ModelRendererHookHarold(ModelBase p_i1173_1_) {
			super(p_i1173_1_);
			// TODO Auto-generated constructor stub
		}

		public void setRender(IRender render) {
			this.render = render;
		}
		
		public IRender getRender() {
			return render;
		}

		@Override
		public void render(float p_78785_1_) {
			if (!this.isHidden)
			{
				if (this.showModel)
				{

					GlStateManager.translate(this.offsetX, this.offsetY, this.offsetZ);
					int i;

					if (this.rotateAngleX == 0.0F && this.rotateAngleY == 0.0F && this.rotateAngleZ == 0.0F)
					{
						if (this.rotationPointX == 0.0F && this.rotationPointY == 0.0F && this.rotationPointZ == 0.0F)
						{
							getRender().render(p_78785_1_);
						}
						else
						{
							GlStateManager.translate(this.rotationPointX * p_78785_1_, this.rotationPointY * p_78785_1_, this.rotationPointZ * p_78785_1_);

							getRender().render(p_78785_1_);

							GlStateManager.translate(-this.rotationPointX * p_78785_1_, -this.rotationPointY * p_78785_1_, -this.rotationPointZ * p_78785_1_);
						}
					}
					else
					{
						GlStateManager.pushMatrix();
						GlStateManager.translate(this.rotationPointX * p_78785_1_, this.rotationPointY * p_78785_1_, this.rotationPointZ * p_78785_1_);

						if (this.rotateAngleZ != 0.0F)
						{
							GlStateManager.rotate(this.rotateAngleZ * (180F / (float)Math.PI), 0.0F, 0.0F, 1.0F);
						}

						if (this.rotateAngleY != 0.0F)
						{
							GlStateManager.rotate(this.rotateAngleY * (180F / (float)Math.PI), 0.0F, 1.0F, 0.0F);
						}

						if (this.rotateAngleX != 0.0F)
						{
							GlStateManager.rotate(this.rotateAngleX * (180F / (float)Math.PI), 1.0F, 0.0F, 0.0F);
						}

						GL11.glScalef(1.025f, 1.025f, 1.025f);
						getRender().render(p_78785_1_);


						GlStateManager.popMatrix();
					}

					GlStateManager.translate(-this.offsetX, -this.offsetY, -this.offsetZ);
				}
			}
		}

	}

}
